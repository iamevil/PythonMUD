import sqlite3
conn = sqlite3.connect('rooms.sqlite')
c = conn.cursor()
directions = ['north', 'east', 'south', 'west']

def getRoom(id):
	c.execute("SELECT id, name, description FROM room WHERE id=?", (id,))
	t = c.fetchone()
	res = {
		'name': t[1],
		'description': t[2]
	}
	for row in c.execute("SELECT room, target, direction FROM connection WHERE room=? OR target=?", (id, id)):
		if row[0] == id:
			res[directions[row[2]]] = row[1]
		else:
			if row[2] + 2 > 3:
				i = row[2] - 2
			else:
				i = row[2] + 2
			res[directions[i]] = row[0]
	
	return res

room = getRoom(1)
print(room['description'])

while True:
    command = input('> ')
    if command == 'look':
        print(room['description'])
    elif command in directions:
        if command in room:
            room = getRoom(room[command])
            print(room['description'])
        else:
            print("There's nothing...")
    else:
        print('Huh?')